package com.vigionpayment.Models;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class R1Model {
    String string;
    String Location;
    ArrayList<String> arrayList1;
    ArrayList<String> arrayList2;

    public R1Model(String string, String Location) {
        this.string = string;
        this.Location = Location;
    }


    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public ArrayList<String> getArrayList1() {
        return arrayList1;
    }

    public void setArrayList1(ArrayList<String> arrayList1) {
        this.arrayList1 = arrayList1;
    }

    public ArrayList<String> getArrayList2() {
        return arrayList2;
    }

    public void setArrayList2(ArrayList<String> arrayList2) {
        this.arrayList2 = arrayList2;
    }
}
