package com.vigionpayment.Models;

/**
 * Created by Bilal Khan on 26/7/17.
 */

public class UIModel {
    int uiImag;
    String uiName;

    public UIModel(int uiImag, String uiName) {
        this.uiImag = uiImag;
        this.uiName = uiName;
    }

    public int getUiImag() {
        return uiImag;
    }

    public void setUiImag(int uiImag) {
        this.uiImag = uiImag;
    }

    public String getUiName() {
        return uiName;
    }

    public void setUiName(String uiName) {
        this.uiName = uiName;
    }
}
