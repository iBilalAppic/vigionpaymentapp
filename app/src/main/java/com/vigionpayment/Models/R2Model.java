package com.vigionpayment.Models;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class R2Model {
    String hall;
    String d23d;
    ArrayList<String> stringArrayList_Time;

    public R2Model(String hall, String d23d) {
        this.hall = hall;
        this.d23d = d23d;

    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public String getD23d() {
        return d23d;
    }

    public void setD23d(String d23d) {
        this.d23d = d23d;
    }

    public ArrayList<String> getStringArrayList_Time() {
        return stringArrayList_Time;
    }

    public void setStringArrayList_Time(ArrayList<String> stringArrayList_Time) {
        this.stringArrayList_Time = stringArrayList_Time;
    }
}
