package com.vigionpayment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.vigionpayment.Adapter.TransactionHistoryAdaper;
import com.vigionpayment.R;

public class TransactionHistory extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    RecyclerView historyRecycler;
    LinearLayoutManager linearLayoutManager;
    TransactionHistoryAdaper historyAdaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_transaction_history);

        init();
    }

    private void init() {

        backBtnImg = (ImageView) findViewById(R.id.backImg);
        historyRecycler = (RecyclerView) findViewById(R.id.transactionRecycler);
        linearLayoutManager = new LinearLayoutManager(this);
        historyRecycler.setLayoutManager(linearLayoutManager);

        historyAdaper = new TransactionHistoryAdaper(this);
        historyRecycler.setAdapter(historyAdaper);

        backBtnImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == backBtnImg) {
            onBackPressed();
        }
    }
}
