package com.vigionpayment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.vigionpayment.R;

public class Travel extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    TextView FlightTxt, BusTxt, TrainTxt;
    CardView flightCL, busCL, trainCL;
    String travelType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_travel);
        travelType = getIntent().getStringExtra("txt");

        init();
        checkForLayout();
    }

    private void init() {

        backBtnImg = (ImageView) findViewById(R.id.backImg);
        FlightTxt = (TextView) findViewById(R.id.flightTxt);
        BusTxt = (TextView) findViewById(R.id.busTxt);
        TrainTxt = (TextView) findViewById(R.id.trainTxt);

        flightCL = (CardView) findViewById(R.id.flightCL);
        busCL = (CardView) findViewById(R.id.busCL);
        trainCL = (CardView) findViewById(R.id.trainCL);

        FlightTxt.setOnClickListener(this);
        BusTxt.setOnClickListener(this);
        TrainTxt.setOnClickListener(this);
        backBtnImg.setOnClickListener(this);
    }


    public void checkForLayout() {
        if (travelType.toString().equals("Bus")) {
            BusTxt.setBackgroundResource(R.drawable.back);
            FlightTxt.setBackgroundResource(0);
            FlightTxt.setText("Flight");
            TrainTxt.setBackgroundResource(0);
            TrainTxt.setText("Train");
            BusTxt.setTextColor(getResources().getColor(R.color.white));
            FlightTxt.setTextColor(getResources().getColor(R.color.lgrey));
            TrainTxt.setTextColor(getResources().getColor(R.color.lgrey));
            flightCL.setVisibility(View.GONE);
            busCL.setVisibility(View.VISIBLE);
            trainCL.setVisibility(View.GONE);


        } else if (travelType.toString().equals("Train")) {
            TrainTxt.setBackgroundResource(R.drawable.back);
            FlightTxt.setBackgroundResource(0);
            FlightTxt.setText("Flight");
            BusTxt.setBackgroundResource(0);
            BusTxt.setText("Bus");
            TrainTxt.setTextColor(getResources().getColor(R.color.white));
            FlightTxt.setTextColor(getResources().getColor(R.color.lgrey));
            BusTxt.setTextColor(getResources().getColor(R.color.lgrey));

            flightCL.setVisibility(View.GONE);
            busCL.setVisibility(View.GONE);
            trainCL.setVisibility(View.VISIBLE);
        } else if (travelType.equals("Flight")) {
            FlightTxt.setBackgroundResource(R.drawable.back);
            BusTxt.setBackgroundResource(0);
            BusTxt.setText("Bus");
            TrainTxt.setBackgroundResource(0);
            TrainTxt.setText("Train");
            FlightTxt.setTextColor(getResources().getColor(R.color.white));
            BusTxt.setTextColor(getResources().getColor(R.color.lgrey));
            TrainTxt.setTextColor(getResources().getColor(R.color.lgrey));
            flightCL.setVisibility(View.VISIBLE);
            busCL.setVisibility(View.GONE);
            trainCL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }
        if (view == FlightTxt) {
            FlightTxt.setBackgroundResource(R.drawable.back);
            BusTxt.setBackgroundResource(0);
            BusTxt.setText("Bus");
            TrainTxt.setBackgroundResource(0);
            TrainTxt.setText("Train");
            FlightTxt.setTextColor(getResources().getColor(R.color.white));
            BusTxt.setTextColor(getResources().getColor(R.color.lgrey));
            TrainTxt.setTextColor(getResources().getColor(R.color.lgrey));
            flightCL.setVisibility(View.VISIBLE);
            busCL.setVisibility(View.GONE);
            trainCL.setVisibility(View.GONE);
        }
        if (view == BusTxt) {

            BusTxt.setBackgroundResource(R.drawable.back);
            FlightTxt.setBackgroundResource(0);
            FlightTxt.setText("Flight");
            TrainTxt.setBackgroundResource(0);
            TrainTxt.setText("Train");
            BusTxt.setTextColor(getResources().getColor(R.color.white));
            FlightTxt.setTextColor(getResources().getColor(R.color.lgrey));
            TrainTxt.setTextColor(getResources().getColor(R.color.lgrey));
            flightCL.setVisibility(View.GONE);
            busCL.setVisibility(View.VISIBLE);
            trainCL.setVisibility(View.GONE);
        }
        if (view == TrainTxt) {

            TrainTxt.setBackgroundResource(R.drawable.back);
            FlightTxt.setBackgroundResource(0);
            FlightTxt.setText("Flight");
            BusTxt.setBackgroundResource(0);
            BusTxt.setText("Bus");
            TrainTxt.setTextColor(getResources().getColor(R.color.white));
            FlightTxt.setTextColor(getResources().getColor(R.color.lgrey));
            BusTxt.setTextColor(getResources().getColor(R.color.lgrey));


            flightCL.setVisibility(View.GONE);
            busCL.setVisibility(View.GONE);
            trainCL.setVisibility(View.VISIBLE);
        }
    }
}
