package com.vigionpayment.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.vigionpayment.Adapter.ImageMovieAdapter;
import com.vigionpayment.Adapter.PageAdapter;
import com.vigionpayment.R;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

public class ShowTimes extends AppCompatActivity {
    DiscreteScrollView discreteScrollView;
    ViewPager viewPager2;
    int ImagesArray[] = {R.drawable.the_dark_knight, R.drawable.thor__ragnarok, R.drawable.wonder_women};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_show_times);

        discreteScrollView = (DiscreteScrollView) findViewById(R.id.picker);

//
//        private DiscreteScrollView itemPicker;
//        SplashAdapter splashAdapter;


        ImageMovieAdapter imageMovieAdapter = new ImageMovieAdapter(ShowTimes.this, ImagesArray);
        discreteScrollView.setAdapter(imageMovieAdapter);
        discreteScrollView.setOffscreenItems(3);
        discreteScrollView.scrollToPosition(1);
        discreteScrollView.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Thu\n17"));
        tabLayout.addTab(tabLayout.newTab().setText("Fri\n18"));
        tabLayout.addTab(tabLayout.newTab().setText("Sat\n20"));
//        tabLayout.addTab(tabLayout.newTab().setText("Sun 21"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.orange));
        tabLayout.setBackgroundColor(Color.TRANSPARENT);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange));

        viewPager2 = (ViewPager) findViewById(R.id.MoviewDetail);
        PageAdapter adapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager2.setAdapter(adapter);
        viewPager2.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager2.setOffscreenPageLimit(7);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
//                        if (Constants.PAGER_VIEW == 0 || Constants.PAGER_VIEW == 1 || Constants.PAGER_VIEW == 2) {
//                            viewPager.setCurrentItem(Constants.PAGER_VIEW);
//                        } else {
//                            viewPager.setCurrentItem(0);
////                        }
//                        animateFab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }
}
