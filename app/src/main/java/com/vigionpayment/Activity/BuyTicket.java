package com.vigionpayment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.vigionpayment.R;

public class BuyTicket extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_buy_ticket);

        init();
    }

    private void init() {
        backBtnImg = (ImageView) findViewById(R.id.backImg);
        backBtnImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }
    }
}
