package com.vigionpayment.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.vigionpayment.Adapter.BookFromWalletUIAdapter;
import com.vigionpayment.Adapter.RechargeAndPayUIAdapter;
import com.vigionpayment.Adapter.WalletUIAdapter;
import com.vigionpayment.Models.UIModel;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.AppPreferance;
import com.vigionpayment.OtherClass.HexagonMaskView;
import com.vigionpayment.OtherClass.UtilsClass;
import com.vigionpayment.R;
import com.vigionpayment.Service.RequestGenerator;
import com.vigionpayment.Service.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeWithNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

//    LinearLayout payLL, receiveLL, passbookLL, mobileLL, electricityLL, dthLL, movieLL, flightLL, busLL;

    RecyclerView walletRecycler, rechAndPayRecycler, bookWithWalletRecycler;
    WalletUIAdapter walletUIAdapter;
    RechargeAndPayUIAdapter rechargeAndPayUIAdapter;
    BookFromWalletUIAdapter bookFromWalletUIAdapter;
    private static long back_pressed;
    private AppPreferance prefrence;
    String userid;
    com.vigionpayment.OtherClass.HexagonMaskView userProfilePic;
    TextView userName, userAmount;
    String profilePic, userNameee, userWalletAmt;
    NavigationView navigationView;
    private View header;
    Menu nav_Menu;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_homewithnav);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();


    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {


        prefrence = new AppPreferance();
        walletRecycler = (RecyclerView) findViewById(R.id.walletRecycler);
        rechAndPayRecycler = (RecyclerView) findViewById(R.id.rechargePayRecycler);
        bookWithWalletRecycler = (RecyclerView) findViewById(R.id.bookFromWalletRecycler);
        walletRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rechAndPayRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        bookWithWalletRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        nav_Menu = navigationView.getMenu();
        header = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        userProfilePic = (com.vigionpayment.OtherClass.HexagonMaskView) header.findViewById(R.id.userPic);
        userName = (TextView) header.findViewById(R.id.userName);
        userAmount = (TextView) header.findViewById(R.id.userWalletAmount);

        userid = prefrence.getPreferences(getApplicationContext(), AppConstant.userId);
        UIModel wallet[] = new UIModel[]
                {
                        new UIModel(R.drawable.ic_rupe_symbol, "Pay"),
                        new UIModel(R.drawable.ic_rupe_symbol, "Receive"),
                        new UIModel(R.drawable.ic_passbook, "Passbook"),
                        new UIModel(R.drawable.ic_rupe_symbol, "Add"),
                };

        walletUIAdapter = new WalletUIAdapter(getApplicationContext(), wallet);
        walletRecycler.setAdapter(walletUIAdapter);

        UIModel RechAndPay[] = new UIModel[]
                {
                        new UIModel(R.drawable.ic_mobile, "Mobile"),
                        new UIModel(R.drawable.ic_electricity, "Electricity"),
                        new UIModel(R.drawable.ic_dth, "DTH"),
                };
        rechargeAndPayUIAdapter = new RechargeAndPayUIAdapter(getApplicationContext(), RechAndPay);
        rechAndPayRecycler.setAdapter(rechargeAndPayUIAdapter);

        UIModel BookWithWallet[] = new UIModel[]
                {
                        new UIModel(R.drawable.ic_movie, "Movie"),
                        new UIModel(R.drawable.ic_flight, "Flight"),
                        new UIModel(R.drawable.ic_bus, "Bus"),
                        new UIModel(R.drawable.ic_train, "Train"),
                };
        bookFromWalletUIAdapter = new BookFromWalletUIAdapter(getApplicationContext(), BookWithWallet);
        bookWithWalletRecycler.setAdapter(bookFromWalletUIAdapter);
//
//        payLL = (LinearLayout) findViewById(R.id.payLL);
//        receiveLL = (LinearLayout) findViewById(R.id.receiveLL);
//        passbookLL = (LinearLayout) findViewById(R.id.passbookLL);

//        payLL.setOnClickListener(this);
//        receiveLL.setOnClickListener(this);
//        passbookLL.setOnClickListener(this);

        getProfile();


    }

    private void getProfile() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", userid);
            RequestGenerator.makePostRequest(HomeWithNav.this, AppConstant.GetProfile, jsonObject, false, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject = new JSONObject(string);
                        String status = jsonObject.getString("status");
                        String messgae = jsonObject.getString("message");
                        if (status.equals("success")) {

                            profilePic = jsonObject.getString("profilepic");
                            userNameee = jsonObject.getString("firstname") + " " + jsonObject.getString("lastname");
                            userWalletAmt = jsonObject.getString("walletamount");
                            prefrence.setPreferences(getApplicationContext(), AppConstant.phoneNum, jsonObject.getString("phone"));
                            prefrence.setPreferences(getApplicationContext(), AppConstant.walletAmount, jsonObject.getString("walletamount"));
                            prefrence.setPreferences(getApplicationContext(), AppConstant.userName, jsonObject.getString("firstname") + " " + jsonObject.getString("lastname"));
                            Picasso.with(getApplicationContext()).load(profilePic).into(userProfilePic);
                            userName.setText(userNameee);
                            userAmount.setText(getText(R.string.Rs) + " " + userWalletAmt);
                        } else {
                            UtilsClass.showToast(getApplicationContext(), messgae);
                        }

                    }
                }
            });


        } catch (JSONException e) {
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                finish();
                super.onBackPressed();

            } else {
                Snackbar snackbar = Snackbar.make(drawer, "Press once again to close app.", Snackbar.LENGTH_SHORT);
                View snackbarView = snackbar.getView();
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarView.setBackgroundColor(Color.DKGRAY);
                snackbar.show();
                back_pressed = System.currentTimeMillis();

            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(HomeWithNav.this, TransactionHistory.class);
            startActivity(intent);

        } else if (id == R.id.nav_contactus) {

        } else if (id == R.id.nav_support) {

        } else if (id == R.id.nav_logout) {
            UtilsClass.showDialog(HomeWithNav.this, getString(R.string.confirm), getString(R.string.dial_titlelogout), getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    UtilsClass.showToast(HomeWithNav.this, "Successfully Logout");
                    prefrence.clearSharedPreference(HomeWithNav.this);
                    Intent i = new Intent(HomeWithNav.this, SignIn.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();


                }
            }, null, getString(R.string.cancel), null, null, false);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
//        if (view == payLL) {
//            Intent intent = new Intent(HomeWithNav.this, Pay.class);
//            startActivity(intent);
//        }
//        if (view == payLL) {
////            Intent intent = new Intent(HomeWithNav.this, Receive.class);
////            startActivity(intent);
//        }
    }
}
