package com.vigionpayment.Activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.AppPreferance;
import com.vigionpayment.R;

/**
 * Created by Ankit on 02-Aug-17.
 */

public class Receive extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg, shareImg;
    ImageView barcodeImage;
    MultiFormatWriter multiFormatWriter;
    String barecodephoneno;
    AppPreferance appPreferance;

    TextView userName, userMMobile;
    String userNm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.receive_layout);
        appPreferance = new AppPreferance();
        userNm = appPreferance.getPreferences(Receive.this, AppConstant.userName);
        barecodephoneno = appPreferance.getPreferences(Receive.this, AppConstant.phoneNum);

        barcodeImage = (ImageView) findViewById(R.id.barcodeImage);
        backBtnImg = (ImageView) findViewById(R.id.backImg);
        shareImg = (ImageView) findViewById(R.id.share);
        userName = (TextView) findViewById(R.id.userKaNaam);
        userMMobile = (TextView) findViewById(R.id.userKaNumber);

        if (!userNm.toString().equals("")) {
            userName.setText("To Pay" + " " + userNm);
            userMMobile.setText(barecodephoneno);
        }

        multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(barecodephoneno, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            barcodeImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        backBtnImg.setOnClickListener(this);
        shareImg.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }
        if (view == shareImg) {

        }

    }
}
