package com.vigionpayment.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ravindu1024.indicatorlib.ViewPagerIndicator;
import com.vigionpayment.Adapter.SimplePagerAdapter;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.AppPreferance;
import com.vigionpayment.R;

import java.util.ArrayList;

public class Walkthrough extends AppCompatActivity {

    ViewPager pager;
    ViewPagerIndicator indicator;
    ArrayList<String> list = new ArrayList<>();
    private static final int PERMISSION_GPS_CODE = 12;
    String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.READ_CONTACTS, Manifest.permission.CAMERA};
    LinearLayout llGetStarted, llGetStarted1;
    String isLogin;
    AppPreferance preferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        preferance = new AppPreferance();
        isLogin = preferance.getPreferences(getApplicationContext(), AppConstant.isLogin);
        checkGPSPermission();

    }

    private void checkGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(permissions, PERMISSION_GPS_CODE);
            } else {
                init();
            }
        } else {
            init();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GPS_CODE) {
            if (grantResults.length > 0) {
                boolean permissionGranted = false;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(Walkthrough.this, getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        permissionGranted = true;
                    }
                }

                if (permissionGranted) {
                    init();
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void init() {
        pager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (ViewPagerIndicator) findViewById(R.id.pager_indicator);

        llGetStarted = (LinearLayout) findViewById(R.id.llButton);
        llGetStarted1 = (LinearLayout) findViewById(R.id.llButton1);
        llGetStarted1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogin.equals("")) {
                    Intent intent = new Intent(Walkthrough.this, SignIn.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Walkthrough.this, HomeWithNav.class);
                    startActivity(intent);
                }
            }
        });
        llGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogin.equals("")) {
                    Intent intent = new Intent(Walkthrough.this, SignIn.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Walkthrough.this, HomeWithNav.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        list.add("One");
        list.add("Two");

        SimplePagerAdapter adapter = new SimplePagerAdapter(this, list);
        pager.setAdapter(adapter);

        indicator.setPager(pager);


        list.add("Three");
        adapter.notifyDataSetChanged();


        pager.setCurrentItem(0);
    }

}
