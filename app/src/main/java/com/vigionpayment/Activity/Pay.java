package com.vigionpayment.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.vigionpayment.Adapter.PayUIAdapter;
import com.vigionpayment.Models.UIModel;
import com.vigionpayment.R;

import java.util.List;

public class Pay extends AppCompatActivity implements PayUIAdapter.onClickL {


    RecyclerView recyclerView;
    PayUIAdapter payUIAdapter;
    CompoundBarcodeView barcodeView;
    private static final int PERMISSION_GPS_CODE = 12;
    String[] permissions = new String[]{Manifest.permission.READ_CONTACTS};

    private Camera camera;
    Button ButtonFlash;
    private boolean isFlashOn = false;
    private boolean hasFlash = false;
    Camera.Parameters params;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pay);

        ButtonFlash = (Button) findViewById(R.id.ButtonFlash);

        barcodeView = (CompoundBarcodeView) findViewById(R.id.cameraid);
        barcodeView.decodeContinuous(callback);
        barcodeView.setStatusText("");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerPay);
        UIModel wallet[] = new UIModel[]
                {
                        new UIModel(R.drawable.ic_contact, "Contact"),
                        new UIModel(R.drawable.ic_phone_call, "Phone No."),
                };

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        payUIAdapter = new PayUIAdapter(getApplicationContext(), wallet, Pay.this);
        recyclerView.setAdapter(payUIAdapter);

        ButtonFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFlashOn) {
                    ButtonFlash.setText("Flash On");

                    barcodeView.setTorchOff();
                    isFlashOn = false;
                } else {
                    ButtonFlash.setText("Flash Off");
                    barcodeView.setTorchOn();
                    isFlashOn = true;
                }
            }
        });


    }


    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {
                barcodeView.setStatusText(result.getText());


                Intent intent = new Intent(getApplicationContext(), PayWithNumber.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("phone", result.getText());
                intent.putExtra("sent", "scan");
                startActivity(intent);


            }

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
            System.out.println("Possible Result points = " + resultPoints);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        barcodeView.resume();
//        if (hasFlash)
//            turnOnFlash();
    }

    @Override
    protected void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // on stop release the camera
        if (camera != null) {
            camera.release();
            camera = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public void itemClick(String s) {
        if (s.equals("Contact")) {
            checkGPSPermission();
        }


        if (s.equals("Phone No.")) {
            Intent intent = new Intent(getApplicationContext(), PayWithNumber.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("phone", "");
            intent.putExtra("sent", "phone");
            startActivity(intent);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    private void checkGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(permissions, PERMISSION_GPS_CODE);
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && reqCode == 1) {
            ContentResolver cr = getContentResolver();

            Uri contactData = data.getData();
            Cursor c = managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Intent intent = new Intent(getApplicationContext(), PayWithNumber.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("phone", phone);
                        intent.putExtra("sent", "contact");
                        startActivity(intent);

                    }
                }
            }


        }
    }
}
