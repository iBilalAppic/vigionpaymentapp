package com.vigionpayment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.vigionpayment.R;

public class DTH extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    private static String[] dth_operator = {"Choose Operator", "Airtel DTH", "DishTv", "Jio TV", "Reliance Digital", "Videocon D2H"};
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dth);

        init();
    }

    private void init() {
        backBtnImg = (ImageView) findViewById(R.id.backImg);
        spinner = (Spinner) findViewById(R.id.spinnerdth);

        ArrayAdapter<String> adp = new ArrayAdapter<String>
                (this, R.layout.spinner_item, dth_operator);
        spinner.setAdapter(adp);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner.getSelectedItem().equals("Choose Operator")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lgrey));

                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        backBtnImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }

    }
}
