package com.vigionpayment.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vigionpayment.R;

public class OTPScreen extends AppCompatActivity implements View.OnClickListener {

    EditText edtOtp;
    Button one, two, three, four, five, six, seven, eight, nine, zero, clear, ok;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otpscreen);


        init();
    }

    private void init() {
        edtOtp = (EditText) findViewById(R.id.optEdit);
        one = (Button) findViewById(R.id.buttonOne);
        two = (Button) findViewById(R.id.buttonTwo);
        three = (Button) findViewById(R.id.buttonThree);
        four = (Button) findViewById(R.id.buttonFour);
        five = (Button) findViewById(R.id.buttonFive);
        six = (Button) findViewById(R.id.buttonSix);
        seven = (Button) findViewById(R.id.buttonSeven);
        eight = (Button) findViewById(R.id.buttonEight);
        nine = (Button) findViewById(R.id.buttonNine);
        zero = (Button) findViewById(R.id.buttonZero);
        clear = (Button) findViewById(R.id.buttonClear);
        ok = (Button) findViewById(R.id.buttonOk);

        edtOtp.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        clear.setOnClickListener(this);
        ok.setOnClickListener(this);
        edtOtp.setInputType(InputType.TYPE_NULL);
    }

    @Override
    public void onClick(View v) {
        if (v == one) {
            edtOtp.setText(edtOtp.getText() + "1");
        }
        if (v == two) {
            edtOtp.setText(edtOtp.getText() + "2");
        }
        if (v == three) {
            edtOtp.setText(edtOtp.getText() + "3");
        }
        if (v == four) {
            edtOtp.setText(edtOtp.getText() + "4");
        }
        if (v == five) {
            edtOtp.setText(edtOtp.getText() + "5");
        }
        if (v == six) {
            edtOtp.setText(edtOtp.getText() + "6");
        }
        if (v == seven) {
            edtOtp.setText(edtOtp.getText() + "7");
        }
        if (v == eight) {
            edtOtp.setText(edtOtp.getText() + "8");
        }
        if (v == nine) {
            edtOtp.setText(edtOtp.getText() + "9");
        }
        if (v == zero) {
            edtOtp.setText(edtOtp.getText() + "0");
        }
        if (v == clear) {
            Editable editableText = edtOtp.getEditableText();
            int length = editableText.length();
            if (length > 0) {
                editableText.delete(length - 1, length);
            }
        }
        if (v == ok) {
            Toast.makeText(OTPScreen.this, "OTP Is : " + edtOtp.getText().toString(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(OTPScreen.this, HomeWithNav.class);
            startActivity(intent);
            edtOtp.setText("");
        }

    }
}
