package com.vigionpayment.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.AppPreferance;
import com.vigionpayment.OtherClass.UtilsClass;
import com.vigionpayment.R;
import com.vigionpayment.Service.RequestGenerator;
import com.vigionpayment.Service.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class SignIn extends AppCompatActivity implements View.OnClickListener {

    TextView txtSingIn, textSignup;
    EditText phoneNum, password;
    AppPreferance preferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
        preferance = new AppPreferance();
        init();
    }

    private void init() {

        txtSingIn = (TextView) findViewById(R.id.txtSingIn);
        textSignup = (TextView) findViewById(R.id.signUp);
        phoneNum = (EditText) findViewById(R.id.userEdt);
        password = (EditText) findViewById(R.id.passwordEdt);

        txtSingIn.setOnClickListener(this);
        textSignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == txtSingIn) {
            if (UtilsClass.isNetworkConnected(getApplicationContext())) {
                if (phoneNum.getText().toString().equals("")) {
                    phoneNum.setError("Phone Number Required");
                } else if (password.getText().toString().equals("")) {
                    password.setError("Password Required");
                } else {
                    LoginService();
                }
            }

        }
        if (view == textSignup) {
            Intent intent = new Intent(SignIn.this, CreateAccount.class);
            startActivity(intent);
        }
    }

    private void LoginService() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", phoneNum.getText().toString());
            jsonObject.put("password", password.getText().toString());
            RequestGenerator.makePostRequest(SignIn.this, AppConstant.Login, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Network Issue");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        Log.e("Login Success", string);
                        JSONObject jsonObject1 = new JSONObject(string);
                        String status = jsonObject1.getString("status");
                        String messgae = jsonObject1.getString("message");
                        if (status.equals("success")) {

                            preferance.setPreferences(getApplicationContext(), AppConstant.isLogin, "login");
                            preferance.setPreferences(getApplicationContext(), AppConstant.userId, jsonObject1.getString("userid"));
                            preferance.setPreferences(getApplicationContext(), AppConstant.phoneNum, jsonObject1.getString("phone"));

                            Intent intent = new Intent(SignIn.this, HomeWithNav.class);
                            startActivity(intent);
                            finish();
                        } else {
                            UtilsClass.showToast(getApplicationContext(), messgae);
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

//
//

    }
}
