package com.vigionpayment.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.AppPreferance;
import com.vigionpayment.OtherClass.UtilsClass;
import com.vigionpayment.R;
import com.vigionpayment.Service.RequestGenerator;
import com.vigionpayment.Service.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class PayWithNumber extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    String phoneNumber, callType, amountRemain, userNumber;
    EditText phoneEdit, amountEdit;
    TextView procedTxt, amountBal;
    AppPreferance preferance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pay_with_number);

        preferance = new AppPreferance();

        amountRemain = (preferance.getPreferences(getApplicationContext(), AppConstant.walletAmount));
        userNumber = (preferance.getPreferences(getApplicationContext(), AppConstant.phoneNum));
        callType = getIntent().getStringExtra("sent");
        if (callType.toString().equals("scan") || callType.toString().equals("contact")) {
            phoneNumber = getIntent().getStringExtra("phone");
        } else {
            phoneNumber = "";
        }

        init();
    }

    private void init() {

        backBtnImg = (ImageView) findViewById(R.id.backImg);
        phoneEdit = (EditText) findViewById(R.id.numEdt);
        amountEdit = (EditText) findViewById(R.id.amountFromEdt);
        procedTxt = (TextView) findViewById(R.id.payNowText);
        amountBal = (TextView) findViewById(R.id.balanceAmt);

        amountBal.setText(getText(R.string.Rs) + " " + amountRemain);
        phoneEdit.setText(phoneNumber);


        procedTxt.setOnClickListener(this);
        backBtnImg.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }
        if (view == procedTxt) {
            if (phoneEdit.getText().toString().equals("")) {
                phoneEdit.setError("Phone Number Required");
            } else if (phoneEdit.getText().toString().length() < 10) {
                phoneEdit.setError("Valid Number Required");
            } else if (amountEdit.getText().toString().equals("")) {
                amountEdit.setError("Please Enter Amount");
            } else {
                PayNowService();
            }

        }
    }

    private void PayNowService() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sendermobile", userNumber);
            jsonObject.put("receivermobile", phoneEdit.getText().toString());
            jsonObject.put("amount", amountEdit.getText().toString());
            Log.e("Pay Req", jsonObject.toString());
            RequestGenerator.makePostRequest(PayWithNumber.this, AppConstant.SendMoney, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    JSONObject jsonObject = new JSONObject(string);
                    Log.e("Pay Res", string);
                    String status = jsonObject.getString("status");
                    String messgae = jsonObject.getString("message");
                    if (status.equals("success")) {

                        Intent intent = new Intent(PayWithNumber.this, HomeWithNav.class);
                        startActivity(intent);
                        finish();
                    } else {
                        UtilsClass.showToast(getApplicationContext(), messgae);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
