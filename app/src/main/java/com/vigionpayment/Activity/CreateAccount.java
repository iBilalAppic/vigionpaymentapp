package com.vigionpayment.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.vigionpayment.OtherClass.AppConstant;
import com.vigionpayment.OtherClass.HexagonMaskView;
import com.vigionpayment.OtherClass.UtilsClass;
import com.vigionpayment.R;
import com.vigionpayment.Service.RequestGenerator;
import com.vigionpayment.Service.ResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CreateAccount extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    com.vigionpayment.OtherClass.HexagonMaskView profilePic;
    ImageButton editImgBtn;
    EditText firstnameEdt, lastNameEdt, addresEdt, phoneNuEdt;
    TextView dateOfBirthEdt;
    TextView nextBtnTxt, hintTxt;


    private DatePickerDialog dateOFEvnet;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_create_account);

        init();

    }

    private void init() {

        backBtnImg = (ImageView) findViewById(R.id.backImg);
        profilePic = (com.vigionpayment.OtherClass.HexagonMaskView) findViewById(R.id.user_profile_photo);
        editImgBtn = (ImageButton) findViewById(R.id.editImgBtn);
        firstnameEdt = (EditText) findViewById(R.id.firstNameEdit);
        lastNameEdt = (EditText) findViewById(R.id.lastNameEdit);
        addresEdt = (EditText) findViewById(R.id.addressEdit);
        phoneNuEdt = (EditText) findViewById(R.id.mobileEdit);
        hintTxt = (TextView) findViewById(R.id.tint);
        dateOfBirthEdt = (TextView) findViewById(R.id.dateOfBirthEdit);

        nextBtnTxt = (TextView) findViewById(R.id.nextTxt);

        backBtnImg.setOnClickListener(this);
        editImgBtn.setOnClickListener(this);
        nextBtnTxt.setOnClickListener(this);
        dateOfBirthEdt.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == backBtnImg) {
            onBackPressed();
        }


        if (view == dateOfBirthEdt) {

            Calendar newCalendar = Calendar.getInstance();
            dateOFEvnet = new DatePickerDialog(CreateAccount.this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                    dateOfBirthEdt.setText(dateFormatter.format(newDate.getTime()));
                    hintTxt.setVisibility(View.VISIBLE);
                }

            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            dateOFEvnet.show();
        }

        if (view == nextBtnTxt) {

            if (UtilsClass.isNetworkConnected(getApplicationContext())) {

                if (firstnameEdt.getText().toString().equals("")) {
                    firstnameEdt.setError("First Name Required");
                } else if (lastNameEdt.getText().toString().equals("")) {
                    lastNameEdt.setError("Last Name Required");
                } else if (addresEdt.getText().toString().equals("")) {
                    addresEdt.setError("Address Required");
                } else if (phoneNuEdt.getText().toString().equals("") || phoneNuEdt.getText().toString().length() > 10) {
                    phoneNuEdt.setError("Valid Phone Number Required");
                } else if (dateOfBirthEdt.getText().toString().equals("")) {
                    dateOfBirthEdt.setError("DOB Required");
                } else {
                    showOtpScreen();
                }

            }

//
//
        }
    }

    private void showOtpScreen() {


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("firstname", firstnameEdt.getText().toString());
            jsonObject.put("lastname", lastNameEdt.getText().toString());
            jsonObject.put("address", addresEdt.getText().toString());
            jsonObject.put("phone", phoneNuEdt.getText().toString());
            jsonObject.put("birthday", dateOfBirthEdt.getText().toString());

            Log.e("Register Requ", jsonObject.toString());

            RequestGenerator.makePostRequest(CreateAccount.this, AppConstant.SignUp, jsonObject, true, new ResponseListener() {
                @Override
                public void onError(VolleyError error) {
                    UtilsClass.showToast(getApplicationContext(), "Some Issue Occured");
                }

                @Override
                public void onSuccess(String string) throws JSONException {
                    if (!string.equals("")) {
                        JSONObject jsonObject = new JSONObject(string);
                        String status = jsonObject.getString("status");
                        String messgae = jsonObject.getString("message");
                        if (status.equals("success")) {
                            Intent intent = new Intent(CreateAccount.this, SignIn.class);
                            startActivity(intent);
                            finish();
                        } else {
                            UtilsClass.showToast(getApplicationContext(), messgae);
                        }
                    }
                }
            });


        } catch (JSONException e) {


        }

    }


}
