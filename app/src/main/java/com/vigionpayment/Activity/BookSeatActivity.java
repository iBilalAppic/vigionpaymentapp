package com.vigionpayment.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.vigionpayment.R;

/**
 * Created by Ankit on 03-Aug-17.
 */

public class BookSeatActivity extends AppCompatActivity {

    LinearLayout bookTl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookingseat_layout);

        bookTl = (LinearLayout) findViewById(R.id.book);

        bookTl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BookSeatActivity.this, BuyTicket.class);
                startActivity(intent);

            }
        });
    }
}
