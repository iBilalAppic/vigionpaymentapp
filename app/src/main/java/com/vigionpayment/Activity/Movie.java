package com.vigionpayment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.vigionpayment.Adapter.MovieAdapter;
import com.vigionpayment.OtherClass.SpacesItemDecoration;
import com.vigionpayment.R;

import static java.security.AccessController.getContext;

public class Movie extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    RecyclerView movieRecycler;
    MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_movie);

        init();
    }

    private void init() {
        backBtnImg = (ImageView) findViewById(R.id.backImg);
        movieRecycler = (RecyclerView) findViewById(R.id.recyclerMovie);
        movieRecycler.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false));
//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.margin_vsmall);
//        movieRecycler.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        movieAdapter = new MovieAdapter(this);
        movieRecycler.setAdapter(movieAdapter);

        backBtnImg.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == backBtnImg) {
            onBackPressed();
        }
    }
}
