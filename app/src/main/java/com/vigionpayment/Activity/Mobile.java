package com.vigionpayment.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vigionpayment.R;

import static android.R.attr.data;

public class Mobile extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtnImg;
    ImageButton getContact;
    TextView prepTxt, postTxt, chosOpe;
    EditText mobileNumEdt, amountEdt;
    private static final int PERMISSION_GPS_CODE = 12;
    String[] permissions = new String[]{Manifest.permission.READ_CONTACTS};
    private static String[] mobile_operator = {"Choose Operator", "Aircel", "Airtel", "BSNL", "Idea", "Jio", "MTNL", "Reliance GSM", "Reliance Mobile", "Tata Docomo", "Vodafone"};
    Spinner spinner;

    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mobile);

        init();
    }

    private void init() {

        backBtnImg = (ImageView) findViewById(R.id.backImg);
        prepTxt = (TextView) findViewById(R.id.prepaidText);
        postTxt = (TextView) findViewById(R.id.postpaidText);
        spinner = (Spinner) findViewById(R.id.spinner1);
        chosOpe = (TextView) findViewById(R.id.choosOper);
        getContact = (ImageButton) findViewById(R.id.getContact);
        mobileNumEdt = (EditText) findViewById(R.id.mobileNumEdt);
        amountEdt = (EditText) findViewById(R.id.amountEdt);

        mobileNumEdt.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        backBtnImg.setOnClickListener(this);
        prepTxt.setOnClickListener(this);
        postTxt.setOnClickListener(this);
        getContact.setOnClickListener(this);

        ArrayAdapter<String> adp = new ArrayAdapter<String>
                (this, R.layout.spinner_item, mobile_operator);
        spinner.setAdapter(adp);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner.getSelectedItem().equals("Choose Operator")) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lgrey));
                    chosOpe.setVisibility(View.GONE);

                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.black));
                    chosOpe.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == backBtnImg) {
            onBackPressed();
        }

        if (view == prepTxt) {
            prepTxt.setBackgroundResource(R.drawable.back);
            postTxt.setBackgroundResource(0);
            postTxt.setText("Postpaid");
            prepTxt.setTextColor(getResources().getColor(R.color.white));
            postTxt.setTextColor(getResources().getColor(R.color.lgrey));

            mobileNumEdt.setText("");
            amountEdt.setText("");

        }
        if (view == postTxt) {
            prepTxt.setText("Prepaid");
            postTxt.setTextColor(getResources().getColor(R.color.white));
            prepTxt.setTextColor(getResources().getColor(R.color.lgrey));
            prepTxt.setBackgroundResource(0);
            postTxt.setBackgroundResource(R.drawable.back);

            mobileNumEdt.setText("");
            amountEdt.setText("");
        }
        if (view == getContact) {
            checkGPSPermission();
        }

    }


    public void checkGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!allPermissionsGiven()) {
                requestPermissions(permissions, PERMISSION_GPS_CODE);
            } else {
                FetchContactBook();
            }
        } else {
            FetchContactBook();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_GPS_CODE) {
            if (grantResults.length > 0) {
                boolean permissionGranted = false;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(Mobile.this, getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                        break;
                    } else {
                        permissionGranted = true;
                    }
                }

                if (permissionGranted) {
                    FetchContactBook();
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean allPermissionsGiven() {

        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void FetchContactBook() {

        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && reqCode == 1) {
            ContentResolver cr = getContentResolver();

            Uri contactData = data.getData();
            Cursor c = managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        mobileNumEdt.setText(phone);
                    }
                }
            }


        }
    }


}
