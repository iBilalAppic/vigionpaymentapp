package com.vigionpayment.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vigionpayment.Adapter.TwoDThreeDAdapter;
import com.vigionpayment.Models.R1Model;
import com.vigionpayment.R;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class DetailMovieFrgment extends Fragment {

    RecyclerView recyclerView;
    TwoDThreeDAdapter twoDThreeDAdapter;
    ArrayList<R1Model> r1Models = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        r1Models = new ArrayList<>();
        r1Models.add(new R1Model("Inox Theater", "Malad"));
        r1Models.add(new R1Model("Maxus Mall ", "Bhayander"));
        r1Models.add(new R1Model("Fun Cinemas", "Andheri"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewcontainer = inflater.inflate(R.layout.movie_detail_fragment, container, false);

        recyclerView = (RecyclerView) viewcontainer.findViewById(R.id.detailRecycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        twoDThreeDAdapter = new TwoDThreeDAdapter(getActivity(), r1Models);
        recyclerView.setAdapter(twoDThreeDAdapter);


        return viewcontainer;
    }
}
