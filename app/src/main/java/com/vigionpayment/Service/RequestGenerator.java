package com.vigionpayment.Service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bilal Khan on 16/6/17.
 */

public class RequestGenerator {

    private static ProgressDialog progressDialog;
    private static int COUNT;

    public static void showProgressDialog(final Activity context) {
        try {
            cancelProgressDialog();
            context.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    COUNT++;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void cancelProgressDialog() {
        try {
            if (progressDialog != null) {
                if (COUNT <= 1) {
                    progressDialog.cancel();
                    progressDialog = null;
                }
                COUNT--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makePostRequest1(Context context, String url, JSONObject body, boolean showDialog, final ResponseListener responseListener) {

        try {

            JSONObject jsonObject = new JSONObject(String.valueOf(body));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(final JSONObject response) {


                            try {
                                Runnable progressRunnable = new Runnable() {

                                    @Override
                                    public void run() {


                                        try {
                                            Log.d("Success Report", response.toString());

                                            responseListener.onSuccess(response.toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        cancelProgressDialog();

                                    }
                                };

                                Handler pdCanceller = new Handler();
                                pdCanceller.postDelayed(progressRunnable, 2000);


                            } catch (Exception e) {

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error Report", error.toString());
                            responseListener.onError(error);
                            cancelProgressDialog();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void makePostRequest(final Activity context, String url, JSONObject body, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }

            JSONObject jsonObject = new JSONObject(String.valueOf(body));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                responseListener.onSuccess(response.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            cancelProgressDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            responseListener.onError(error);
                            cancelProgressDialog();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 50, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 10, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT * 10));
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void makeGetRequest(final Activity context, String url, boolean showDialog, final ResponseListener responseListener) {

        try {
            if (showDialog) {
                showProgressDialog(context);
            }
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        responseListener.onSuccess(response.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cancelProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    responseListener.onError(error);
                    error.printStackTrace();
                    cancelProgressDialog();
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                        volleyError = error;
                    }
                    return volleyError;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 30, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 5, 0));

            VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
