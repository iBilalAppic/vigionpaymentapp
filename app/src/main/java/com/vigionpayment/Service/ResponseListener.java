package com.vigionpayment.Service;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by Bilal on 9/04/2017.
 */
public interface ResponseListener {
    void onError(VolleyError error);

    void onSuccess(String string) throws JSONException;
}
