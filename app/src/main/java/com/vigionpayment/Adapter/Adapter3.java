package com.vigionpayment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vigionpayment.Activity.BookSeatActivity;
import com.vigionpayment.R;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class Adapter3 extends RecyclerView.Adapter<Adapter3.ViewHolder> {

    private ArrayList<String> data;
    private Context context;


    public Adapter3(Context applicationContext, ArrayList<String> data) {
        this.context = applicationContext;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) {
            holder.linearlayout.setBackground(context.getResources().getDrawable(R.drawable.time_not_present));
            holder.time.setText(data.get(position));
            holder.time.setTextColor(context.getResources().getColor(R.color.timenotavble));
        } else {
            holder.linearlayout.setBackground(context.getResources().getDrawable(R.drawable.time_present));
            holder.time.setText(data.get(position));
            holder.time.setTextColor(context.getResources().getColor(R.color.orange));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView time;
        LinearLayout linearlayout;

        public ViewHolder(View view) {
            super(view);

            time = (TextView) view.findViewById(R.id.time);
            linearlayout = (LinearLayout) view.findViewById(R.id.linearlayout);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, BookSeatActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }
    }
}
