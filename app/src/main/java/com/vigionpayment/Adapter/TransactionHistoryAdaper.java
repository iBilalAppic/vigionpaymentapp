package com.vigionpayment.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vigionpayment.Activity.TransactionHistory;
import com.vigionpayment.R;

/**
 * Created by Bilal Khan on 28/7/17.
 */

public class TransactionHistoryAdaper extends RecyclerView.Adapter<TransactionHistoryAdaper.ViewHolder> {

    Context context;

    public TransactionHistoryAdaper(TransactionHistory transactionHistory) {
        this.context = transactionHistory;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_ui, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
