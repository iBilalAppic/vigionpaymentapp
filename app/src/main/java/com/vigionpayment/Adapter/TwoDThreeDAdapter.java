package com.vigionpayment.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vigionpayment.Models.R1Model;
import com.vigionpayment.Models.R2Model;
import com.vigionpayment.R;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class TwoDThreeDAdapter extends RecyclerView.Adapter<TwoDThreeDAdapter.ViewHolder> {

    private ArrayList<R1Model> data;
    private Context context;


    public TwoDThreeDAdapter(Context applicationContext, ArrayList<R1Model> data) {
        this.context = applicationContext;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r1item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        R1Model uiModel = data.get(position);

        holder.textView_Theater.setText(uiModel.getString());
        holder.textView2_Location.setText(uiModel.getLocation());

        ArrayList<R2Model> data2 = new ArrayList<>();
        data2.add(new R2Model("Hall#1", "2D"));
        data2.add(new R2Model("Hall#2", "3D"));
        data2.add(new R2Model("Hall#3", "4D"));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        Adapter2 adapter2 = new Adapter2(context, data2);

        holder.recyclerView.setAdapter(adapter2);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RecyclerView recyclerView;
        TextView textView_Theater, textView2_Location;

        public ViewHolder(View view) {
            super(view);

            recyclerView = (RecyclerView) view.findViewById(R.id.recycler2);
            textView_Theater = (TextView) view.findViewById(R.id.textView_Theater);
            textView2_Location = (TextView) view.findViewById(R.id.textView2_Location);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
        }
    }
}
