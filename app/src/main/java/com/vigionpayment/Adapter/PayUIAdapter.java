package com.vigionpayment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vigionpayment.Activity.Pay;
import com.vigionpayment.Activity.PayWithNumber;
import com.vigionpayment.Models.UIModel;
import com.vigionpayment.R;

/**
 * Created by Ankit on 02-Aug-17.
 */

public class PayUIAdapter extends RecyclerView.Adapter<PayUIAdapter.ViewHolder> {

    private UIModel[] data;
    private Context context;
    private onClickL listener;

//    public WalletUIAdapter(Context applicationContext, ArrayList<UIModel> data) {
//        this.context = applicationContext;
//        this.data = data;
//    }

    public PayUIAdapter(Context applicationContext, UIModel[] weather_data, onClickL listener) {
        this.context = applicationContext;
        this.data = weather_data;
        this.listener = listener;
    }

    @Override
    public PayUIAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_ui, parent, false);
        return new PayUIAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PayUIAdapter.ViewHolder holder, int position) {

        UIModel uiModel = data[position];

        holder.tv_android.setText(uiModel.getUiName());
        Picasso.with(context).load(uiModel.getUiImag()).placeholder(R.drawable.ic_menu_camera).into(holder.img_android);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_android;
        private ImageView img_android;

        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView) view.findViewById(R.id.name);
            img_android = (ImageView) view.findViewById(R.id.iconIma);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (view == itemView) {
                listener.itemClick(tv_android.getText().toString());
            }
        }
    }


    public interface onClickL {

        void itemClick(String s);

    }


}
