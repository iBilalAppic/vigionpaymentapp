package com.vigionpayment.Adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vigionpayment.Fragment.DetailMovieFrgment;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class PageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {

        switch (position) {
            case 0:
                DetailMovieFrgment tab1 = new DetailMovieFrgment();
                return tab1;
            case 1:
                DetailMovieFrgment tab2 = new DetailMovieFrgment();
                return tab2;
            case 2:
                DetailMovieFrgment tab3 = new DetailMovieFrgment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}