package com.vigionpayment.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.vigionpayment.R;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class ImageMovieAdapter extends RecyclerView.Adapter<ImageMovieAdapter.ViewHolder> {

    private int data[];
    private Context context;


    public ImageMovieAdapter(Context applicationContext, int data[]) {
        this.context = applicationContext;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.imagesmovie_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.imagemoview.setImageResource(data[position]);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imagemoview;

        public ViewHolder(View view) {
            super(view);

            imagemoview = (ImageView) view.findViewById(R.id.imagemoview);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
        }
    }
}
