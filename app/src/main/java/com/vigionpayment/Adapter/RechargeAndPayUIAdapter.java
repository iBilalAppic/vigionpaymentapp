package com.vigionpayment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vigionpayment.Activity.DTH;
import com.vigionpayment.Activity.Electricity;
import com.vigionpayment.Activity.Mobile;
import com.vigionpayment.Models.UIModel;
import com.vigionpayment.R;

/**
 * Created by Bilal Khan on 26/7/17.
 */

public class RechargeAndPayUIAdapter extends RecyclerView.Adapter<RechargeAndPayUIAdapter.ViewHolder> {

    private UIModel[] data;
    private Context context;


    public RechargeAndPayUIAdapter(Context applicationContext, UIModel[] weather_data) {
        this.context = applicationContext;
        this.data = weather_data;
    }

    @Override
    public RechargeAndPayUIAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_ui, parent, false);
        return new RechargeAndPayUIAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UIModel uiModel = data[position];

        holder.tv_android.setText(uiModel.getUiName());
        Picasso.with(context).load(uiModel.getUiImag()).placeholder(R.drawable.ic_menu_camera).into(holder.img_android);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_android;
        private ImageView img_android;

        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView) view.findViewById(R.id.name);
            img_android = (ImageView) view.findViewById(R.id.iconIma);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (view == itemView) {
                if (tv_android.getText().toString().equals("Mobile")) {
                    Intent intent = new Intent(context, Mobile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (tv_android.getText().toString().equals("DTH")) {
                    Intent intent = new Intent(context, DTH.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (tv_android.getText().toString().equals("Electricity")) {
                    Intent intent = new Intent(context, Electricity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }


            }


        }
    }


}