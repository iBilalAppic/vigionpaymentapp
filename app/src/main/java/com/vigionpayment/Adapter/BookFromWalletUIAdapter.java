package com.vigionpayment.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vigionpayment.Activity.Electricity;
import com.vigionpayment.Activity.Movie;
import com.vigionpayment.Activity.Travel;
import com.vigionpayment.Models.UIModel;
import com.vigionpayment.R;

/**
 * Created by Bilal Khan on 26/7/17.
 */

public class BookFromWalletUIAdapter extends RecyclerView.Adapter<BookFromWalletUIAdapter.ViewHolder> {

    private UIModel[] data;
    private Context context;

    public BookFromWalletUIAdapter(Context applicationContext, UIModel[] bookWithWallet) {
        this.context = applicationContext;
        this.data = bookWithWallet;
    }

    @Override
    public BookFromWalletUIAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_ui, parent, false);
        return new BookFromWalletUIAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UIModel uiModel = data[position];

        holder.tv_android.setText(uiModel.getUiName());
        Picasso.with(context).load(uiModel.getUiImag()).placeholder(R.drawable.ic_menu_camera).into(holder.img_android);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_android;
        private ImageView img_android;

        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView) view.findViewById(R.id.name);
            img_android = (ImageView) view.findViewById(R.id.iconIma);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                if (tv_android.getText().toString().equals("Movie")) {
                    Intent intent = new Intent(context, Movie.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (tv_android.getText().toString().equals("Flight")) {
                    Intent intent = new Intent(context, Travel.class);
                    intent.putExtra("txt", "Flight");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (tv_android.getText().toString().equals("Bus")) {
                    Intent intent = new Intent(context, Travel.class);
                    intent.putExtra("txt", "Bus");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (tv_android.getText().toString().equals("Train")) {
                    Intent intent = new Intent(context, Travel.class);
                    intent.putExtra("txt", "Train");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        }
    }
}
