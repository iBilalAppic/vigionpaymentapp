package com.vigionpayment.Adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vigionpayment.Models.R2Model;
import com.vigionpayment.R;

import java.util.ArrayList;

/**
 * Created by Ankit on 04-Aug-17.
 */

public class Adapter2 extends RecyclerView.Adapter<Adapter2.ViewHolder> {

    private ArrayList<R2Model> data;
    private Context context;


    public Adapter2(Context applicationContext, ArrayList<R2Model> data) {
        this.context = applicationContext;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        R2Model uiModel = data.get(position);

        holder.textView_Theater_Hall.setText(uiModel.getHall() + " " + uiModel.getD23d());
        ArrayList<String> strings = new ArrayList<>();

        strings.add("10:30 AM");
        strings.add("01:30 PM");
        strings.add("05:00 PM");
        strings.add("08:00 PM");
        strings.add("11:00 PM");

        GridLayoutManager linearLayoutManager = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        Adapter3 adapter3 = new Adapter3(context, strings);
        holder.recyclerView.setAdapter(adapter3);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RecyclerView recyclerView;
        TextView textView_Theater_Hall;

        public ViewHolder(View view) {
            super(view);

            recyclerView = (RecyclerView) view.findViewById(R.id.recycler3);
            textView_Theater_Hall = (TextView) view.findViewById(R.id.textView_Theater_Hall);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
        }
    }
}
