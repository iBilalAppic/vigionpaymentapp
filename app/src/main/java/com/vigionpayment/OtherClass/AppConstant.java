package com.vigionpayment.OtherClass;

/**
 * Created by Bilal Khan on 3/8/17.
 */

public class AppConstant {
    public static String MainUrl = "http://35.154.228.79/beaconpayment/mobileservice.php?servicename=";

    //    public static String SignUp = MainUrl + "";
    public static String SignUp = MainUrl + "register";
    public static String Login = MainUrl + "login";
    public static String GetProfile = MainUrl + "getprofile";
    public static String SendMoney = MainUrl + "sendwalletmoney";

    public static String deviceToken = "devicetoken";
    public static String userId = "userid";
    public static final String isLogin = "login";
    public static String phoneNum = "phone";
    public static final String userName = "user";
    public static String walletAmount = "profilepic";


    public static String notification_key_message = "message";
    public static String notification_key_type = "type";
    public static String notification_key_name = "title";
    public static String notification_key_number = "number";
    public static String notification_key_server_id = "server_contactId";
    public static String notification_key_image_url = "image";
}
